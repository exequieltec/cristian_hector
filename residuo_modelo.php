<?php
require_once('conexion.php');
class Residuo_model
{
	private $pdo; //driver de conexion a la base de datos

public function __construct(){}
  //------------------------------------------- LISTADO DE TODOS LOS TIPOS DE DOCUMENTOS -----------------------
    public function Listar()
	{
		try
		{
			$result= array(); //crea un array

			$stm= $this ->pdo-> prepare ("SELECT * FROM `residuo`");

			$stm-> execute(); //ejecuta la consulta

			foreach ($stm->fetchAll (PDO::FETCH_OBJ) as $r)
			{
				$residuo= new Residuo(); //crea una instancia de area_entidad

				$residuo-> set_idResiduo($r-> idResiduo);
				$residuo-> set_nombre($r-> nombre);
				$residuo-> set_fecha($r-> fecha);
				$residuo-> set_usuario($r-> usuario);
				$residuo-> set_id($r-> id);
				
				$result[]= $residuo;
			}

			return $result;

		}catch(Exception $e){
			die($e->getMessage());

		}
	}
}
?>
