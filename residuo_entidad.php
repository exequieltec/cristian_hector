<?php

class Residuo{

	private $idResiduo;
	private $nombre;
	private $fecha;
	private $usuario;
	private $idresiduoestado;
	private $estado;


	public function set_idResiduo($valor) { $this->idResiduo = $valor; }
	public function set_estado($valor) { $this->estado = $valor; }
	public function set_nombre($valor) { $this->nombre = $valor; }
	public function set_fecha($valor) { $this->fecha = $valor; }
	public function set_usuario($valor) { $this->usuario = $valor; }
	public function set_idresiduoestado($valor) { $this->idresiduoestado = $valor; }

	public function get_idResiduo() {return $this->idResiduo; }
	public function get_estado() {return $this->estado; }
	public function get_nombre() {return $this->nombre; }
	public function get_fecha() {return $this->fecha; }
	public function get_usuario() {return $this->usuario; }
	public function get_idresiduoestado() {return $this->idresiduoestado; }
}

?>