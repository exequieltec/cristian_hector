-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-06-2019 a las 13:40:01
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `residuotipo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `residuo`
--

CREATE TABLE `residuo` (
  `idResiduo` int(20) NOT NULL,
  `nombre` varchar(155) NOT NULL,
  `estado` varchar(255) NOT NULL,
  `tipo` varchar(155) NOT NULL,
  `fecha` text NOT NULL,
  `idresiduoestado` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `residuo`
--

INSERT INTO `residuo` (`idResiduo`, `nombre`, `estado`, `tipo`, `fecha`, `idresiduoestado`) VALUES
(41, 'asceite', 'Por salir', 'No peligroso', '2000-11-15', 0),
(47, 'metal', 'por salir', 'peligroso', '12-09', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `residuoestado`
--

CREATE TABLE `residuoestado` (
  `idresiduoestado` int(3) NOT NULL,
  `estado` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `residuoestado`
--

INSERT INTO `residuoestado` (`idresiduoestado`, `estado`) VALUES
(1, 'entregado'),
(2, 'por salir'),
(3, 'disposision final'),
(4, 'En transito');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `residuotipo`
--

CREATE TABLE `residuotipo` (
  `idResiduoTipo` int(3) NOT NULL,
  `tipo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `residuotipo`
--

INSERT INTO `residuotipo` (`idResiduoTipo`, `tipo`) VALUES
(1, 'Reciclable'),
(2, 'peligroso'),
(3, 'No peligroso');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `residuo`
--
ALTER TABLE `residuo`
  ADD PRIMARY KEY (`idResiduo`);

--
-- Indices de la tabla `residuoestado`
--
ALTER TABLE `residuoestado`
  ADD PRIMARY KEY (`idresiduoestado`);

--
-- Indices de la tabla `residuotipo`
--
ALTER TABLE `residuotipo`
  ADD PRIMARY KEY (`idResiduoTipo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `residuo`
--
ALTER TABLE `residuo`
  MODIFY `idResiduo` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de la tabla `residuoestado`
--
ALTER TABLE `residuoestado`
  MODIFY `idresiduoestado` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `residuotipo`
--
ALTER TABLE `residuotipo`
  MODIFY `idResiduoTipo` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
