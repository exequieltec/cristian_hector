<?php
include_once '../conexion.php';
//leer
$sql_leer = 'SELECT * FROM  residuoestado';

$gsent = $pdo->prepare($sql_leer);

$gsent->execute();

$resultado = $gsent->fetchAll();

//var_dump($resultado);

//AGREGAR

	if ($_POST) {
	$id = $_POST['id'];
	$estado = $_POST['estado'];

	$sql_agregar = 'INSERT INTO residuoestado(idresiduoestado,estado) VALUES (?,?)';
	$sentencia_agregar = $pdo->prepare($sql_agregar);
	$sentencia_agregar->execute(array($idresiduoestado,$estado)); 

 header('location:estado de residuo.php');
}

if($_GET){
	$estado =$_GET['estado'];

$sql_unico = 'SELECT * FROM  residuoestado WHERE estado=?';

$gsent_unico = $pdo->prepare($sql_unico);

$gsent_unico->execute(array($estado));

$resultado_unico = $gsent_unico->fetch();

//var_dump($resultado_unico);
}
	
?>

<!doctype html>
<html lang="en">
  <head>
	<link rel="shortcut icon" href="../imagenes/reciclable.png" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <title>Gestion de Residuos</title>
    <style type="text/css">
    	body{
    		background:        linear-gradient(
          rgba(0, 0, 0, 0.7), 
          rgba(0, 0, 0, 0.7)
        );
    		background-image:url('../imagenes/residuos.jpg');
    		background-repeat:no-repeat;
    		background-attachment: fixed;
			background-size: cover;
    	}
		#contenedor{
			margin:auto;
			width:1000px; 
		}
		 #encabezado{
		 	text-align: center;
		 	margin:50px 0px 70px 0px;
		 	border: 2px solid red;
		 	border-radius: 20px;
		 	background: lightgray;
		 }
		 #cuerpo{
		 	border: 2px solid red;
		 	margin:0px 0px 150px 0px ;
		 	border-radius: 20px;
		 	background: lightgray;
		 }
		 #atras{
		 	float: left;
		 }
    </style>
  </head>
  <body>
  	<div id="contenedor">
  	<div id="encabezado">
  		<a href="../index.php"><img id="atras" src="../imagenes/flecha.png" width="50" height="50"></a>
  		<h1>Estado de Residuos</h1>
  		
  	</div>
  	<div id="cuerpo">
    	<div class="container mt-5">
    		<div class="row">
    			<div class="col-md-6">
    				
    				<?php foreach ($resultado as $dato): ?>

	    			<div 
	    				class="alert alert-primary <?php echo $dato ['idresiduoestado'] ?> text-uppercase"  role="alert">
	 			 		<?php echo $dato ['idresiduoestado']?>
	 			 		-
	 			 		<?php echo $dato ['estado']?>
	 			 		<a href="eliminarestado.php?idresiduoestado=<?php echo $dato ['idresiduoestado'] ?>" class="float-right ml-2">
	 			 			<i class="fas fa-trash"></i>
	 			 		 </a>


	 			 		<a href="estado de residuo.php?estado=<?php echo $dato ['estado'] ?>" class="float-right">
	 			 			<i class="far fa-edit"></i>
	 			 		</a>
					</div>
 						<?php endforeach ?>
					
				</div>
				<div class="col-md-6">
					<?php if (!$_GET): ?>
					<center>
					<h2>AGREGAR ESTADO DE RESIDUO</h2>
					</center>
					<fieldset>
					<form method="POST" action="estado de residuo.php">
						<input type="hidden" class="form-control" name="idresiduoestado">
						<input type="text" class="form-control mt-3" name="estado">
						<center>
						<button class="btn btn-primary mt-3">Agregar</button>
					</center>
					</form>
				</fieldset>
				<?php endif ?>
				
				

				<?php if ($_GET): ?>
					<center>
					<h2>EDITAR RESIDUOS</h2>
					</center>
					<fieldset>
					<form method="GET" action="editarestado.php">
						<input type="hidden" class="form-control" name="idresiduoestado" value=<?php echo $resultado_unico['idresiduoestado']?>>
						<input type="text" class="form-control mt-3" name="estado" value=<?php echo $resultado_unico['estado']?>>

						<input type="hidden" name="idresiduoestado" value=<?php echo $resultado_unico['idresiduoestado']?> >
						<center>

						<button class="btn btn-primary mt-3">Guardar</button>
					</center>
					</form>
				</fieldset>
				<?php endif ?>

				</div>	

				</div>	

			</div>	
    	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</div>
</div>
</body>
</html>
<?php 
//cerramos conexion bd y sentencias
$pdo = null;
$gsent = null;

?>