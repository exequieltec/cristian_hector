<?php 
include_once '../conexion.php';
//leer
$sql_leer = 'SELECT * FROM  residuotipo';

$gsent = $pdo->prepare($sql_leer);

$gsent->execute();

$resultado = $gsent->fetchAll();

//var_dump($resultado);

//AGREGAR

	if ($_POST) {
	$idResiduoTipo = $_POST['idResiduoTipo'];
	$tipo = $_POST['tipo'];

	$sql_agregar = 'INSERT INTO residuotipo(idResiduoTipo,tipo) VALUES (?,?)';
	$sentencia_agregar = $pdo->prepare($sql_agregar);
	$sentencia_agregar->execute(array($idResiduoTipo,$tipo)); 

 header('location:tipo de residuo.php');
}

if($_GET){
	$tipo =$_GET['tipo'];

$sql_unico = 'SELECT * FROM  residuotipo WHERE tipo=?';

$gsent_unico = $pdo->prepare($sql_unico);

$gsent_unico->execute(array($tipo));

$resultado_unico = $gsent_unico->fetch();

//var_dump($resultado_unico);
}
	
?>

<!doctype html>
<html lang="en">
  <head>
	<link rel="shortcut icon" href="../imagenes/reciclable.png" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <title>Gestion de Residuos</title>
    <style type="text/css">
    	body{
    		background:        linear-gradient(
          rgba(0, 0, 0, 0.7), 
          rgba(0, 0, 0, 0.7)
        );
    		background-image:url('../imagenes/plasticos.jpg');
    		background-repeat:no-repeat;
    		background-attachment: fixed;
			background-size: cover;
    	}
		#contenedor{
			margin:auto;
			width:1000px; 
		}
		 #encabezado{
		 	text-align: center;
		 	margin:50px 0px 70px 0px;
		 	border: 2px solid red;
		 	border-radius: 20px;
		 	background: lightgray;
		 }
		 #cuerpo{
		 	background: lightgray;
		 	border: 2px solid red;
		 	margin:0px 0px 150px 0px ;
		 	border-radius: 20px;
		 }
		 #atras{
		 	float: left;
		 }
    </style>
  </head>
  <body>
  	<div id="contenedor">
  	<div id="encabezado">
  		<a href="../index.php"><img id="atras" src="../imagenes/flecha.png" width="50" height="50"></a>
  		<h1>Tipos de Residuos</h1>
  		
  	</div>
  	<div id="cuerpo">
    	<div class="container mt-5">
    		<div class="row">
    			<div class="col-md-6">
    				
    				<?php foreach ($resultado as $dato): ?>

	    			<div 
	    				class="alert alert-primary <?php echo $dato ['idResiduoTipo'] ?> text-uppercase"  role="alert">
	 			 		<?php echo $dato ['idResiduoTipo']?>
	 			 		-
	 			 		<?php echo $dato ['tipo']?>
	 			 		<a href="eliminar.php?idResiduoTipo=<?php echo $dato ['idResiduoTipo'] ?>" class="float-right ml-2">
	 			 			<i class="fas fa-trash"></i>
	 			 		 </a>


	 			 		<a href="tipo de residuo.php?tipo=<?php echo $dato ['tipo'] ?>" class="float-right">
	 			 			<i class="far fa-edit"></i>
	 			 		</a>
					</div>
 						<?php endforeach ?>
					
				</div>
				<div class="col-md-6">
					<?php if (!$_GET): ?>
					<center>
					<h2>AGREGAR RESIDUO</h2>
					</center>
					<fieldset>
					<form method="POST" action="tipo de residuo.php">
						<input type="hidden" class="form-control" name="idResiduoTipo">
						<input type="" class="form-control mt-3" name="tipo">
						<center>
						<button class="btn btn-primary mt-3">Agregar</button>
					</center>
					</form>
				</fieldset>
				<?php endif ?>

				<?php if ($_GET): ?>
					<center>
					<h2>EDITAR RESIDUOS</h2>
					</center>
					<fieldset>
					<form method="GET" action="editar.php">
						<input type="hidden" class="form-control" name="idResiduoTipo" value=<?php echo $resultado_unico['idResiduoTipo']?>>
						<input type="text" class="form-control mt-3" name="tipo" value=<?php echo $resultado_unico['tipo']?>>

						<input type="hidden" name="idResiduoTipo" value=<?php echo $resultado_unico['idResiduoTipo']?> >
						<center>

						<button class="btn btn-primary mt-3">Guardar</button>
					</center>
					</form>
				</fieldset>
				<?php endif ?>

				</div>	

				</div>	

			</div>	
    	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</div>
</div>
</body>
</html>
<?php 
//cerramos conexion bd y sentencias
$pdo = null;
$gsent = null;

?>